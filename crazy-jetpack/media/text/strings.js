var _STRINGS = {
	"Ad":{
		"Mobile":{
			"Preroll":{
				"ReadyIn":"The game is ready in ",
				"Loading":"Your game is loading...",
				"Close":"Close",
			},
			"Header":{
				"ReadyIn":"The game is ready in ",
				"Loading":"Your game is loading...",
				"Close":"Close",
			},
			"End":{
				"ReadyIn":"Advertisement ends in ",
				"Loading":"Please wait ...",
				"Close":"Close",
			},								
		},
	},
	
	"Splash":{
		"Loading":"Loading ...",	
		"LogoLine1":"Some text here",
		"LogoLine2":"powered by MarketJS",
		"LogoLine3":"none",		
        "TapToStart":"TAP TO START",				
	},

	"Game":{
		"SelectPlayer":"Select Player",
		"Win":"You win!",
		"Lose":"You lose!",
		"Score":"Score",
		"Time":"Time",
	},

	"Results":{
		"Title":"High score",
	},

	"Tutorial":{
		'Init':'Tap to fly jetpack. Collect bonuses. Avoid obstacles!'
	},

	"Game":{
		'Next':'Continue',
		'Distance':'Distance',
		'Obstacle':'Obstacle',
		'Coins':'Coins',
		'Play':'PLAY',
		'MoreGames':'MORE GAMES',
		'Paused':'PAUSED',
		'Results':'RESULTS',
		'TapToRevive':'TAP TO REVIVE',
		'Start':'START',
		'Shop':'SHOP',
		'BGMON':'BGM ON',
		'BGMOFF':'BGM OFF',
		'SFXON':'SFX ON',
		'SFXOFF':'SFX OFF',
		'Home':'HOME',
		'Retry':'RETRY',
		'DescMagnet':'Coin Magnet Radius',
		'DescBubble':'Invincibility Bubble Duration',
		'DescDouble':'Double Coin Duration',
		'DescForward':'Fast Forward',
		'DescHeart':'Extra Lives'
	}


};